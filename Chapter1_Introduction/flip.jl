using Plots
using Distributions
using StatPlots

@enum RESULT heads tails

n_trials = [0, 1, 2, 3, 4, 5, 8, 15, 50, 500]
run_trials(n) = [rand([heads, tails]) for _ in 1:n]

plots = []
x = range(0, stop=1, length=100)
for results in map(run_trials, n_trials)
    num_heads = count(x -> x == heads, results)
    α = 1 + num_heads
    β = 1 + length(results) - num_heads
    p = plot(pdf.(Beta(α, β), x), fill=(0, .5, :blue))
    push!(plots, p)
end

p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 = plots
display(plot(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, layout=(5, 2), legend=false))

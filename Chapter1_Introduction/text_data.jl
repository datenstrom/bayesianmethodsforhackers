"""
FIXME: Bug causing paramater names λ₁ and λ₂ to fail when used
       as keys `:λ₁`. Switched to p1 and p2.

       It looks like it also prevents psummary_λ₁ from working...

"""

using Turing, DelimitedFiles, Distributions, StatPlots, Statistics

count_data = readdlm("data/textdata.csv")
bar(count_data)


# FIXME: Something is wrong here...
#        psummary looks strange Union{Missing, Float64}
@model text_data(y, b) = begin
    # Unknowns
    α = 1 / mean(count_data)
    # Observations
    τ ~ DiscreteUniform(0, b)
    lambda_1 ~ Exponential(α)
    lambda_2 ~ Exponential(α)
    
    for n in 1:length(y)
        if τ > n
            y[n] ~ Poisson(lambda_1)
        else
            y[n] ~ Poisson(lambda_2)
        end
    end
end


N = length(count_data)
chain = sample(text_data(count_data, N), HMC(10000, 0.05, 10))
lambda_1_samples = chain[:lambda_1]
lambda_2_samples = chain[:lambda_2]
psummary_lambda_1 = Chains(chain[:lambda_1])
psummary_lambda_2 = Chains(chain[:lambda_2])
#histogram(psummary_p1)
#histogram(psummary_p2)

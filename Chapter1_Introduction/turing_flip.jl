using Turing, StatPlots, Random

# Probability a coin flip is heads
p_true = 0.5

N = 0:100

# Draw data from a Bernoulli distribution
Random.seed!(42)
data = rand(Bernoulli(p_true), last(N))

# Declare the Turing model
@model coinflip(y) = begin
    # Out prior belief about the probability of heads
    p ~ Beta(1, 1)

    # The number of observations
    N = length(y)
    for n in 1:N
        # Heads or tails of a coin are drawn from a Bernoulli distribution
        y[n] ~ Bernoulli(p)
    end
end

iterations = 1000
∈ = 0.05
τ = 10

# Start sampling
chain = sample(coinflip(data), HMC(iterations, ∈, τ))

# Construct summary of the sampling process for the parameter
psummary = Chains(chain[:p])
histogram(psummary)

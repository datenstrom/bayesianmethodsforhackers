using StatPlots, Plots

prior_plot = plot(prior -> 2 * prior / (1 + prior), 0:0.01:1)

priors = [0.20, 0.80]
posteriors = [1/3, 2/3]
groupedbar(hcat(priors, posteriors))

# Bay

[Turing Tutorials](http://turing.ml/tutorials/)

## Graphing Options

- [Plots](http://docs.juliaplots.org/latest/)
- [Gadfly](http://gadflyjl.org/stable/)

## Files

1. `flip.jl`: Return after probability density functions.
2. `prob.jl`
